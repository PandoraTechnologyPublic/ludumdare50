using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace LudumDare50
{
    public class DialogueUiLayout : MonoBehaviour
    {
        [SerializeField] private TextAsset _dialogueFile;
        [SerializeField] private Image _characterImage;
        [SerializeField] private TMP_Text _dialogueText;
        [SerializeField] private Button _dialogueChoiceButtonPrototype;
        [SerializeField] private RectTransform _textPanel;

        private readonly List<Button> _dialogueChoiceButtons = new List<Button>();

        private readonly Dictionary<string, DialogueLine> _dialogueLines = new Dictionary<string, DialogueLine>();
        private InteractableComponent _currentInteractableComponent;
        private UiManager _uiManager;
        private GameManager _gameManager;
        private PlayerController _playerController;
        private Coroutine _dialogueCoroutine;
        private bool _doesSkipTextDisplay;
        private string _dialogueLineReference;

        private void Start()
        {
            _dialogueChoiceButtons.Add(_dialogueChoiceButtonPrototype);

            _uiManager = GameObject.FindObjectOfType<UiManager>();
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            _playerController = GameObject.FindObjectOfType<PlayerController>();

            LoadDialogueFile(_dialogueFile);
        }

        public void CheckDialogueData()
        {
            CheckDialogLines(_dialogueLines);
        }

        private void CheckDialogLines(Dictionary<string,DialogueLine> dialogueLines)
        {
            foreach (KeyValuePair<string, DialogueLine> dialogueLine in dialogueLines)
            {
                foreach (DialogueChoice dialogueChoice in dialogueLine.Value._choices)
                {
                    foreach (DialogueChoice.DialogElement dialogueChoiceCondition in dialogueChoice._conditions)
                    {
                        //:TODO: dialogueChoiceCondition exists ?
                    }

                    foreach (DialogueChoice.DialogElement dialogueChoiceOption in dialogueChoice._options)
                    {
                        switch (dialogueChoiceOption._keyword)
                        {
                            // case KeywordType.KEY_REQUIRED:
                            // {
                            //     break;
                            // }
                            // case KeywordType.TAKE:
                            // {
                            //     break;
                            // }
                            // case KeywordType.GAME_OVER:
                            // {
                            //     break;
                            // }
                            // case KeywordType.UNLOCK_KEY:
                            // {
                            //     break;
                            // }
                            case KeywordType.CONTINUE:
                            {
                                Assert.IsTrue(_dialogueLines.ContainsKey(dialogueChoiceOption._reference), $"Missing Key: {dialogueChoiceOption._reference}");
                                break;
                            }
                            case KeywordType.END:
                            {
                                Assert.IsTrue(_dialogueLines.ContainsKey(dialogueChoiceOption._reference), $"Missing Key: {dialogueChoiceOption._reference}");
                                break;
                            }
                            // case KeywordType.SWAP:
                            // {
                            //     break;
                            // }
                            case KeywordType.ACTIVATE:
                            {
                                Assert.IsTrue(_gameManager.ContainsActivateObject(dialogueChoiceOption._reference),$"Missing Activable Object: {dialogueChoiceOption._reference}");
                                break;
                            }
                            case KeywordType.DEACTIVATE:
                            {
                                Assert.IsTrue(_gameManager.ContainsActivateObject(dialogueChoiceOption._reference),$"Missing Activable Object: {dialogueChoiceOption._reference}");
                                break;
                            }
                            // default:
                            // {
                            //     throw new ArgumentOutOfRangeException();
                            // }
                        }
                    }
                }
            }
        }

        private void LoadDialogueFile(TextAsset dialogFileText)
        {
            char[] separators = { '\r', '\n' };
            string[] lines = dialogFileText.text.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            for (int lineIndex = 1; lineIndex < lines.Length; lineIndex++)
            {
                string lineData = lines[lineIndex];

                if (!string.IsNullOrEmpty(lineData))
                {
                    List<string> lineElements = new List<string>();
                    int currentIndex = 0;

                    while (currentIndex < lineData.Length)
                    {
                        if (lineData[currentIndex] == '"')
                        {
                            int closingQuoteMarkIndex = lineData.IndexOf('"', currentIndex + 1);
                            string substring = lineData.Substring(currentIndex + 1, closingQuoteMarkIndex - (currentIndex + 1));

                            lineElements.Add(substring);

                            currentIndex = closingQuoteMarkIndex + 2;
                        }
                        else
                        {
                            int nextComaIndex = lineData.IndexOf(',', currentIndex);

                            if (nextComaIndex == -1)
                            {
                                nextComaIndex = lineData.Length;
                            }

                            lineElements.Add(lineData.Substring(currentIndex, nextComaIndex - currentIndex));
                            currentIndex = nextComaIndex + 1;
                        }
                    }

                    DialogueLine line = new DialogueLine { _reference = lineElements[0], _characterReference = lineElements[1], _text = lineElements[2] };

                    for (int choiceIndex = 3; choiceIndex < lineElements.Count; choiceIndex++)
                    {
                        string choice = lineElements[choiceIndex];

                        if (!string.IsNullOrEmpty(choice))
                        {
                            DialogueChoice dialogueChoice = new DialogueChoice();
                            int currentChoiceCharacterIndex = 0;

                            if (choice[0] == '[')
                            {
                                int closingBracketIndex = choice.IndexOf(']');
                                string condition = choice.Substring(1, closingBracketIndex - 1);
                                string[] conditionParts = condition.Split(':');

                                Assert.AreEqual(2, conditionParts.Length);

                                KeywordType keyword = Enum.Parse<KeywordType>(conditionParts[0]);

                                Assert.IsTrue((keyword == KeywordType.KEY_REQUIRED) || (keyword == KeywordType.HIDE_IF_KEY));

                                string keysText = conditionParts[1];

                                string[] keys = keysText.Split(';');

                                foreach (string key in keys)
                                {
                                    DialogueChoice.DialogElement conditionDialogElement = new DialogueChoice.DialogElement
                                    {
                                        _keyword = keyword,
                                        _reference = key
                                    };

                                    dialogueChoice._conditions.Add(conditionDialogElement);
                                }

                                currentChoiceCharacterIndex = closingBracketIndex + 1;
                            }

                            int textLength = choice.Length - currentChoiceCharacterIndex;
                            int openingBracketIndex = choice.IndexOf('[', currentChoiceCharacterIndex);

                            if (openingBracketIndex != -1)
                            {
                                textLength = openingBracketIndex - currentChoiceCharacterIndex;
                            }

                            dialogueChoice._text = choice.Substring(currentChoiceCharacterIndex, textLength);

                            // Regex regex = new Regex(@"(\[.[^\]\[]*\])?(.[^\]\[]*){1}(\[.[^\]\[]*\])*");
                            // MatchCollection matchCollection = regex.Matches(choice);

                            while (openingBracketIndex != -1)
                            {
                                int closingBracketIndex = choice.IndexOf(']', currentChoiceCharacterIndex);

                                currentChoiceCharacterIndex = openingBracketIndex + 1;

                                string linkOption = choice.Substring(currentChoiceCharacterIndex, closingBracketIndex - currentChoiceCharacterIndex);

                                currentChoiceCharacterIndex = closingBracketIndex + 1;
                                openingBracketIndex = choice.IndexOf('[', currentChoiceCharacterIndex);

                                string[] optionParts = linkOption.Split(':');
                                // Assert.AreEqual(2, optionParts.Length);

                                DialogueChoice.DialogElement optionDialogElement = new DialogueChoice.DialogElement
                                {
                                    _keyword = Enum.Parse<KeywordType>(optionParts[0])
                                };

                                if (optionParts.Length > 1)
                                {
                                    optionDialogElement._reference = optionParts[1];
                                }

                                dialogueChoice._options.Add(optionDialogElement);
                            }

                            line._choices.Add(dialogueChoice);
                        }
                    }

                    _dialogueLines.Add(line._reference, line);
                }
            }
        }

        public void StartDialogue(string dialogueLineReference, InteractableComponent interactableComponent)
        {
            if (_dialogueCoroutine == null)
            {
                _dialogueCoroutine = StartCoroutine(StartDialogueCoroutine(dialogueLineReference, interactableComponent));
            }
        }

        private void Update()
        {
            if (!_doesSkipTextDisplay && Mouse.current.leftButton.wasPressedThisFrame)
            {
                _doesSkipTextDisplay = true;
            }
        }

        private IEnumerator StartDialogueCoroutine(string dialogueLineReference, InteractableComponent interactableComponent, bool doesShowHints = false)
        {
            _doesSkipTextDisplay = false;
            _currentInteractableComponent = interactableComponent;
            _dialogueLineReference = dialogueLineReference;

            DialogueLine dialogLine = _dialogueLines[dialogueLineReference];

            foreach (Button dialogueChoiceButton in _dialogueChoiceButtons)
            {
                dialogueChoiceButton.gameObject.SetActive(false);
            }

            _characterImage.sprite = _uiManager.GetCharacterPortrait(dialogLine._characterReference);

            // bool doesTitleForceItalic = dialogLine._text.StartsWith("<i>--");

            for (int characterIndex = 0; characterIndex <= dialogLine._text.Length; characterIndex++)
            {
                StringBuilder textToDisplay = new StringBuilder();

                // if (doesTitleForceItalic)
                // {
                //     textToDisplay.Append("<size=60%><font=F25_Executive SDF>");
                // }

                string sourceText = dialogLine._text.Substring(0, characterIndex);

                textToDisplay.Append(sourceText);

                // if (doesTitleForceItalic)
                // {
                //     textToDisplay.Append("</font></size>");
                // }

                _dialogueText.SetText(textToDisplay);

                if (!_doesSkipTextDisplay)
                {
                    yield return new WaitForSeconds(GameplayConfig.TEXT_SPEED);
                }
            }

            int choiceIndex = 0;

            for (; choiceIndex < dialogLine._choices.Count; choiceIndex++)
            {
                if (choiceIndex >= _dialogueChoiceButtons.Count)
                {
                    Button choiceButton = GameObject.Instantiate(_dialogueChoiceButtonPrototype, _textPanel);

                    _dialogueChoiceButtons.Add(choiceButton);
                }

                DialogueChoice dialogLineChoice = dialogLine._choices[choiceIndex];

                _dialogueChoiceButtons[choiceIndex].onClick.RemoveAllListeners();
                _dialogueChoiceButtons[choiceIndex].onClick.AddListener(() => OnChoiceButtonSelected(dialogLineChoice));
                _dialogueChoiceButtons[choiceIndex].gameObject.SetActive(true);
                _dialogueChoiceButtons[choiceIndex].interactable = true;

                bool areConditionsFulfilled = true;
                bool shouldBeHiddenAfterKeyUnlocked = false;

                if (dialogLineChoice._conditions.Count > 0)
                {
                    foreach (DialogueChoice.DialogElement dialogElement in dialogLineChoice._conditions)
                    {
                        switch (dialogElement._keyword)
                        {
                            case KeywordType.HIDE_IF_KEY:
                            {
                                if (_gameManager.HasKey(dialogElement._reference))
                                {
                                    shouldBeHiddenAfterKeyUnlocked = true;
                                }

                                break;
                            }
                            case KeywordType.KEY_REQUIRED:
                            {
                                if (!_gameManager.HasKey(dialogElement._reference))
                                {
                                    areConditionsFulfilled = false;
                                }

                                break;
                            }
                            default:
                            {
                                throw new ArgumentOutOfRangeException();
                            }
                        }
                    }
                }

                _dialogueChoiceButtons[choiceIndex].gameObject.SetActive(!shouldBeHiddenAfterKeyUnlocked);

                _dialogueChoiceButtons[choiceIndex].interactable = areConditionsFulfilled;

                StringBuilder stringBuilder = new StringBuilder();

                if (!areConditionsFulfilled)
                {
                    stringBuilder.Append($"<color={GameplayConfig.HIDDEN_TEXT_COLOR}>");
                }

                if (doesShowHints)
                {
                    foreach (DialogueChoice.DialogElement dialogElement in dialogLineChoice._conditions)
                    {
                        stringBuilder.Append($"<size=50%>[{dialogElement._keyword}:{dialogElement._reference}] </size>");
                    }
                }

                if (!areConditionsFulfilled && !doesShowHints)
                {
                    stringBuilder.Append("[HIDDEN]");
                }
                else
                {
                    // bool doesForceItalic = dialogLineChoice._text.StartsWith("<i>--");
                    //
                    // if (doesForceItalic)
                    // {
                    //     stringBuilder.Append("<size=60%><font=F25_Executive SDF>");
                    // }

                    stringBuilder.Append(dialogLineChoice._text);

                    // if (doesForceItalic)
                    // {
                    //     stringBuilder.Append("</font></size>");
                    // }
                }

                if (doesShowHints)
                {
                    foreach (DialogueChoice.DialogElement dialogElement in dialogLineChoice._options)
                    {
                        stringBuilder.Append($"<size=50%>[{dialogElement._keyword}:{dialogElement._reference}] </size>");
                    }
                }

                if (!areConditionsFulfilled)
                {
                    stringBuilder.Append("</color>");
                }

                _dialogueChoiceButtons[choiceIndex].GetComponentInChildren<TMP_Text>().SetText(stringBuilder.ToString());

                if (!_doesSkipTextDisplay)
                {
                    yield return new WaitForSeconds(GameplayConfig.TEXT_SPEED);
                }
            }

            for (; choiceIndex < _dialogueChoiceButtons.Count; choiceIndex++)
            {
                _dialogueChoiceButtons[choiceIndex].gameObject.SetActive(false);
            }

            _dialogueCoroutine = null;
            _doesSkipTextDisplay = false;
        }

        private void OnChoiceButtonSelected(DialogueChoice dialogLineChoice)
        {
            Assert.IsTrue(_gameManager.DoesFulfillConditions(dialogLineChoice._conditions));

            bool doesCloseDialogue = true;

            foreach (DialogueChoice.DialogElement dialogElement in dialogLineChoice._options)
            {
                switch (dialogElement._keyword)
                {
                    case KeywordType.GAME_OVER:
                    {
                        _gameManager.TriggerGameOver();

                        break;
                    }
                    case KeywordType.TAKE:
                    {
                        _currentInteractableComponent.TakeElement();

                        break;
                    }
                    case KeywordType.UNLOCK_KEY:
                    {
                        if (!_gameManager.HasKey(dialogElement._reference))
                        {
                            _gameManager.AddKeyToInventory(dialogElement._reference);
                        }

                        break;
                    }
                    case KeywordType.CONTINUE:
                    {
                        doesCloseDialogue = false;

                        if (_currentInteractableComponent != null)
                        {
                            _currentInteractableComponent.SetNextDialogueReference(dialogElement._reference);
                        }

                        StartDialogue(dialogElement._reference, _currentInteractableComponent);

                        break;
                    }
                    case KeywordType.END:
                    {
                        _currentInteractableComponent.SetNextDialogueReference(dialogElement._reference);

                        break;
                    }
                    case KeywordType.SWAP:
                    {
                        _currentInteractableComponent.GetComponent<SwapComponent>()._elementToSwapWith.gameObject.SetActive(true);

                        _currentInteractableComponent.gameObject.SetActive(false);

                        break;
                    }
                    case KeywordType.ACTIVATE:
                    {
                        _gameManager.ActivateObject(dialogElement._reference);

                        break;
                    }
                    case KeywordType.DEACTIVATE:
                    {
                        _gameManager.DeactivateObject(dialogElement._reference);

                        break;
                    }
                    case KeywordType.KEY_REQUIRED:
                    case KeywordType.HIDE_IF_KEY:
                    default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                }
            }

            if (doesCloseDialogue)
            {
                _uiManager.HideDialog(_playerController);
            }
        }

        public bool HasDialog(string dialogueReference)
        {
            return _dialogueLines.ContainsKey(dialogueReference);
        }

        public void ShowHints()
        {
            _dialogueCoroutine = StartCoroutine(StartDialogueCoroutine(_dialogueLineReference, _currentInteractableComponent, true));
        }
    }
}