namespace LudumDare50
{
    public static class GameplayConfig
    {
        public static readonly string HIDDEN_TEXT_COLOR = "#505050";
        public static float TEXT_SPEED = 0.01f;
        public static readonly float PUBLIC_MAX_WILL = 1000.0f;
        public static readonly float PUBLIC_WILL_LOSING_SPEED = 10.0f;
    }
}