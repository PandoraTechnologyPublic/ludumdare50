using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace LudumDare50
{
    public class GameOverScreenLayout : MonoBehaviour
    {
        [SerializeField] private List<RectTransform> _elementToDisplay;

        private bool _isSkipToMenuActivated;

        private void OnEnable()
        {
            foreach (RectTransform rectTransform in _elementToDisplay)
            {
                rectTransform.gameObject.SetActive(false);
            }

            StartCoroutine(DisplayTextCoroutine());
        }

        private IEnumerator DisplayTextCoroutine()
        {
            _isSkipToMenuActivated = false;

            foreach (RectTransform rectTransform in _elementToDisplay)
            {
                yield return new WaitForSeconds(1.0f);
                rectTransform.gameObject.SetActive(true);
            }

            _isSkipToMenuActivated = true;
        }

        private void Update()
        {
            if (_isSkipToMenuActivated && Mouse.current.leftButton.wasReleasedThisFrame)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}