using System;
using UnityEngine;

namespace LudumDare50
{
    public class ActivableComponent : MonoBehaviour
    {
        public bool _doesStartActivated;
        public string _reference;

        private void Start()
        {
            GameObject.FindObjectOfType<GameManager>().RegisterActivableObject(_reference, this);
            gameObject.SetActive(_doesStartActivated);
        }
    }
}