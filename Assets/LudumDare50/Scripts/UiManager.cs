using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LudumDare50
{
    public class UiManager : MonoBehaviour
    {
        [SerializeField] private Button _hintButton;
        [SerializeField] private RectTransform _gameOverScreenPrefab;
        [SerializeField] private RectTransform _clickCatcher;
        [SerializeField] private TMP_Text _interactionText;
        [SerializeField] private Image _publicWillProgressBar;
        [SerializeField] private List<CharacterDefinition> _characterDefinitionList;

        private DialogueUiLayout _dialogueUiLayout;

        private readonly Dictionary<string, CharacterDefinition> _characterDefinitions = new Dictionary<string, CharacterDefinition>();

        private GameManager _gameManager;

        private IEnumerator Start()
        {
            _dialogueUiLayout = GetComponentInChildren<DialogueUiLayout>();
            _dialogueUiLayout.gameObject.SetActive(false);
            _clickCatcher.gameObject.SetActive(false);

            foreach (CharacterDefinition characterDefinition in _characterDefinitionList)
            {
                _characterDefinitions.Add(characterDefinition._characterReference, characterDefinition);
            }

            _gameManager = GameObject.FindObjectOfType<GameManager>();

            _interactionText.gameObject.SetActive(false);
            _hintButton.gameObject.SetActive(false);
            _hintButton.onClick.AddListener(OnShowHintButton);

            yield return null;

            _dialogueUiLayout.CheckDialogueData();
        }

        private void OnShowHintButton()
        {
            _dialogueUiLayout.ShowHints();
        }

        // private void Update()
        // {
        //     _publicWillProgressBar.fillAmount = _gameManager.GetPublicWillProgress();
        // }

        public void ShowDialog(string dialogueLineReference, InteractableComponent interactableComponent)
        {
            _clickCatcher.gameObject.SetActive(true);
            _dialogueUiLayout.gameObject.SetActive(true);
            _hintButton.gameObject.SetActive(true);
            _dialogueUiLayout.StartDialogue(dialogueLineReference, interactableComponent);
        }

        public void HideDialog(PlayerController playerController)
        {
            _clickCatcher.gameObject.SetActive(false);
            _dialogueUiLayout.gameObject.SetActive(false);
            _hintButton.gameObject.SetActive(false);
            playerController.SetIsInteracting(false);
        }

        public Sprite GetCharacterPortrait(string characterReference)
        {
            Sprite characterPortrait = null;

            if (_characterDefinitions.TryGetValue(characterReference, out CharacterDefinition characterDefinition))
            {
                characterPortrait = characterDefinition._portrait;
            }

            return characterPortrait;
        }

        public void DisplayInteractionText(string text, Vector3 position)
        {
            _interactionText.SetText(text);
            _interactionText.gameObject.SetActive(true);
            ((RectTransform)_interactionText.transform).position =  position;
        }

        public void HideInteractionText()
        {
            _interactionText.gameObject.SetActive(false);
        }

        public void DisplayGameOverScreen()
        {
            GameObject.Instantiate(_gameOverScreenPrefab);
        }
    }
}
