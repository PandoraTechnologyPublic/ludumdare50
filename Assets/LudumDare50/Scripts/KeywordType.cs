namespace LudumDare50
{
    public enum KeywordType
    {
        HIDE_IF_KEY,
        KEY_REQUIRED,
        TAKE,
        GAME_OVER,
        UNLOCK_KEY,
        CONTINUE,
        END,
        SWAP,
        ACTIVATE,
        DEACTIVATE,
    }
}