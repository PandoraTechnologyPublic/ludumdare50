using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

namespace LudumDare50
{
    public class InteractableComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField] private Transform _playerDock;
        [SerializeField] private bool _playerIsFacingLeft;

        private PlayerController _playerController;
        private GameManager _gameManager;
        private UiManager _uiManager;

        private DialogueComponent _dialogueComponent;
        private KeyComponent _keyComponent;

        private void Start()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            _uiManager = GameObject.FindObjectOfType<UiManager>();
            _playerController = GameObject.FindObjectOfType<PlayerController>();
            _dialogueComponent = GetComponent<DialogueComponent>();
            _keyComponent = GetComponent<KeyComponent>();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _uiManager.DisplayInteractionText(name, eventData.position);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _uiManager.HideInteractionText();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_playerController.IsInteracting())
            {
                Vector3 targetPosition = eventData.pointerPressRaycast.gameObject.transform.parent.position;

                if (_playerDock != null)
                {
                    targetPosition = _playerDock.position;
                }

                Debug.DrawLine(targetPosition, targetPosition + Vector3.up * 5, Color.red, 2);

                float distance = Vector3.Distance(_playerController.transform.position, targetPosition);

                if (distance < 1)
                {
                    Trigger();
                }
                else
                {
                    _playerController.SetDestination(targetPosition, this);
                }
            }
        }

        public void Trigger()
        {
            if (_dialogueComponent != null)
            {
                _uiManager.ShowDialog(_dialogueComponent._dialogueReference, this);
                _playerController.SetIsInteracting(true);

                if (name.Contains("Microphone"))
                {
                    _playerController.SetMicrophoneAnimation();
                }

                _playerController.SetOrientation(_playerIsFacingLeft);
            }
            else
            {
                Assert.IsNotNull(_keyComponent);
                Assert.IsFalse(string.IsNullOrEmpty(_keyComponent._keyReference));

                TakeElement();
            }
        }

        public void TakeElement()
        {
            Assert.IsFalse(string.IsNullOrEmpty(_keyComponent._keyReference));

            gameObject.SetActive(false);
            _gameManager.AddKeyToInventory(_keyComponent._keyReference);
        }

        public void SetNextDialogueReference(string nextDialogueReference)
        {
            _dialogueComponent._dialogueReference = nextDialogueReference;
        }
    }
}