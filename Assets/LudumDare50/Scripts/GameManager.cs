using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace LudumDare50
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private AudioSource _gameMusic;
        [SerializeField] private AudioSource _gameOverMusic;

        private float _currentPublicWill;
        private float _currentPublicWillLoseSpeed;

        private readonly HashSet<string> _keys = new HashSet<string>();
        private readonly Dictionary<string, ActivableComponent> _activableElements = new Dictionary<string, ActivableComponent>();
        private UiManager _uiManager;

        private IEnumerator Start()
        {
            _currentPublicWill = GameplayConfig.PUBLIC_MAX_WILL;
            _currentPublicWillLoseSpeed = GameplayConfig.PUBLIC_WILL_LOSING_SPEED;

            _gameOverMusic.Stop();

            yield return null;

            _uiManager = GameObject.FindObjectOfType<UiManager>();
            _uiManager.ShowDialog("GAME_BOOT_START", null);
        }

        // private void Update()
        // {
        //     _currentPublicWill -= _currentPublicWillLoseSpeed * Time.deltaTime;
        //
        //     if (_currentPublicWill < 0)
        //     {
        //         TriggerGameOver();
        //     }
        // }

        public float GetPublicWillProgress()
        {
            return _currentPublicWill / GameplayConfig.PUBLIC_MAX_WILL;
        }

        public void TriggerGameOver()
        {
            _uiManager.DisplayGameOverScreen();
            _gameMusic.Stop();
            _gameOverMusic.Play();
        }

        public void AddKeyToInventory(string keyReference)
        {
            _keys.Add(keyReference);
        }

        public bool HasKey(string neededKeyReference)
        {
            return _keys.Contains(neededKeyReference);
        }

        public void ActivateObject(string objectToActivateReference)
        {
            _activableElements[objectToActivateReference].gameObject.SetActive(true);
        }

        public void DeactivateObject(string objectToDeactivateReference)
        {
            _activableElements[objectToDeactivateReference].gameObject.SetActive(false);
        }

        public void RegisterActivableObject(string reference, ActivableComponent activableComponent)
        {
            _activableElements.Add(reference, activableComponent);
        }

        public bool DoesFulfillConditions(List<DialogueChoice.DialogElement> conditions)
        {
            bool doesFulfillCondition = true;

            foreach (DialogueChoice.DialogElement dialogElement in conditions)
            {
                if (KeywordType.KEY_REQUIRED == dialogElement._keyword)
                {
                    if (!_keys.Contains(dialogElement._reference))
                    {
                        doesFulfillCondition = false;
                    }
                }
            }

            return doesFulfillCondition;
        }

        public bool ContainsActivateObject(string reference)
        {
            return _activableElements.ContainsKey(reference);
        }
    }
}
