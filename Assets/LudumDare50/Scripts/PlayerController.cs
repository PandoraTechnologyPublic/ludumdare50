using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace LudumDare50
{
    public class PlayerController : MonoBehaviour
    {
        private bool _isInteracting = false;

        private NavMeshAgent _navMeshAgent;
        private SpriteRenderer _spriteRenderer;

        private Coroutine _targetMonitoringCoroutine;
        private Animator _animator;

        private static readonly int ON_MIC = Animator.StringToHash("onMic");
        private static readonly int IS_MOVING = Animator.StringToHash("isMoving");

        private void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            _animator = GetComponentInChildren<Animator>();
            _navMeshAgent.updateRotation = false;
            _animator.SetBool(ON_MIC, true);
            _isInteracting = true;
        }

        public bool IsInteracting()
        {
            return _isInteracting;
        }

        public void SetIsInteracting(bool isInteracting)
        {
            _isInteracting = isInteracting;
        }

        public void SetDestination(Vector3 targetPosition, InteractableComponent interactableComponent = null)
        {
            if (_targetMonitoringCoroutine != null)
            {
                StopCoroutine(_targetMonitoringCoroutine);
            }

            _navMeshAgent.SetDestination(targetPosition);

            if (interactableComponent != null)
            {
                _targetMonitoringCoroutine = StartCoroutine(CheckDestinationArrival(targetPosition, interactableComponent));
            }
        }

        private void Update()
        {
            if (!_isInteracting)
            {
                if (_navMeshAgent.velocity.x != 0)
                {
                    _spriteRenderer.flipX = Mathf.Sign(_navMeshAgent.velocity.x) > 0;
                    // _animator.
                    _animator.SetBool(ON_MIC, false);
                }

                _animator.SetBool(IS_MOVING, _navMeshAgent.velocity.x != 0);
            }
            else
            {
                _animator.SetBool(IS_MOVING, false);
            }
        }

        private IEnumerator CheckDestinationArrival(Vector3 targetPosition, InteractableComponent interactableComponent)
        {
            while (Vector3.Distance(transform.position, targetPosition) > 1)
            {
                yield return null;
            }

            interactableComponent.Trigger();
        }

        public void SetMicrophoneAnimation()
        {
            _animator.SetBool(ON_MIC, true);
            // _navMeshAgent.isStopped = true;
        }

        public void SetOrientation(bool playerIsFacingLeft)
        {
            _spriteRenderer.flipX = playerIsFacingLeft;
        }
    }
}